$('.filters__btn').click(function(e) {
  e.preventDefault();
  $('.filters__btn-text').toggleClass('filters__btn-text--active');
  $('.filters__content').slideToggle('fast');
});
