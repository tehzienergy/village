$('.select__input').select2({
  minimumResultsForSearch: -1,
  width: '100%',
  dropdownAutoWidth: true,
  placeholder: function() {
    $(this).data('placeholder');
  }
});
