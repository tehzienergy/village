$('.speaker-select__header').click(function(e) {
  e.preventDefault();
  $('.speaker-select__content').slideDown('fast');
  const ps3 = new PerfectScrollbar('.speaker-select__content');
});

$('.speaker-select__item').click(function(e) {
  e.preventDefault();
  $('.speaker-select__item').removeClass('speaker-select__item--active');
  $(this).addClass('speaker-select__item--active');
  $('.speaker-select__content').slideUp('fast');
});
