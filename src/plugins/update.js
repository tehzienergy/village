$('.tracks__slider').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  infinite: false,
  prevArrow: $('.tracks__slider-arrow--prev'),
  nextArrow: $('.tracks__slider-arrow--next'),
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1
      }
    },
  ]
})
